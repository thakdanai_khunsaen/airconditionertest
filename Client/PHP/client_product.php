<?php

require_once __DIR__ . '/vendor/autoload.php';

$client = new Zend\Soap\Client('http://localhost:51689/AirService.asmx?WSDL');

$client->AddProduct(['id' => "0010",
    'customer_name' => "Mr.Jerry Mouse",
    'address' => "818 Bangkok",
    'weight' => 35.25]);

print_r ($client->getProduct()->getProductResult->ProductData);

$client->SendComplete(['id' => "0010"]);

print_r ($client->getProduct()->getProductResult->ProductData);
?>