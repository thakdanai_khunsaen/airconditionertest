<?php

require_once __DIR__ . '/vendor/autoload.php';

$client = new Zend\Soap\Client('http://localhost:51689/AirService.asmx?WSDL');
$result = $client->AboutMe();

$data = ($result->AboutMeResult);

echo "Name : ", $data->name, "\n";
echo "Type : ", gettype($data->name);
echo "\n\n";

echo "ID : ", $data->id, "\n";
echo "Type : ", gettype($data->id);
echo "\n\n";

echo "Hobby\n";
echo "Type : ", gettype($data->hobby);
echo " : ", gettype($data->hobby->string);
echo "\n\n";

echo "Hobby 1 : ", $data->hobby->string[0], "\n";
echo "Type : ", gettype($data->hobby->string[0]);
echo "\n\n";

echo "Hobby 2 : ", $data->hobby->string[1], "\n";
echo "Type : ", gettype($data->hobby->string[1]);

?>