#import datetime
from zeep import Client

client = Client('http://localhost:51689/AirService.asmx?WSDL')


print("Name : " + client.service.AboutMe()["name"])
print(type(client.service.AboutMe()["name"]))
print()

print("ID : " + client.service.AboutMe()["id"])
print(type(client.service.AboutMe()["id"]))
print()

print("Hobby")
print(type(client.service.AboutMe()["hobby"]))
print()

print("Hobby 1 : " + client.service.AboutMe()["hobby"]["string"][0])
print(type(client.service.AboutMe()["hobby"]["string"][0]))
print()

print("Hobby 2 : " + client.service.AboutMe()["hobby"]["string"][1])
print(type(client.service.AboutMe()["hobby"]["string"][1]))
print()
