﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for AirService
/// </summary>
[WebService(Namespace = "http://test.cpre/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class AirService : System.Web.Services.WebService
{

    public AirService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

//------------------------------ Air Conditioner ------------------------------------------------------
    public class AirData
    {
        public string room_number;
        public string time_record;
        public double temperature;
        public double humidity;
    }


    [WebMethod]
    public void AddData(string room, string time, double temperature, double humidity)
    {
        XElement xml = XElement.Load(AppDomain.CurrentDomain.BaseDirectory + "/air_data.xml");
        xml.Add(new XElement("AirData",
                    new XElement("Room",room),
                    new XElement("Time", time),
                    new XElement("Temperature", temperature),
                    new XElement("Humidity", humidity)
                    ));
        xml.Save(AppDomain.CurrentDomain.BaseDirectory + "/air_data.xml");
    }

    [WebMethod]
    public AirData[] getData()
    {
        XElement xml = XElement.Load(AppDomain.CurrentDomain.BaseDirectory + "/air_data.xml");
        IEnumerable<XElement> query =
            from el in xml.Elements()
            select el;

        AirData[] test = new AirData[query.Count()];

        int i = 0;
        foreach (XElement el in query)
        {   
            test[i] = new AirData()
            {
                room_number = (string)el.Element("Room"),
                time_record = (string)el.Element("Time"),
                temperature = (double)el.Element("Temperature"),
                humidity = (double)el.Element("Humidity")
            };
            i++;
        }
        return test;
    }

    [WebMethod]
    public AirData[] getDataByRoom(string roomNo)
    {
        XElement xml = XElement.Load(AppDomain.CurrentDomain.BaseDirectory + "/air_data.xml");
        IEnumerable<XElement> query =
            from el in xml.Elements()
            where (string)el.Element("Room") == roomNo
            select el;

        AirData[] test = new AirData[query.Count()];

        int i = 0;
        foreach (XElement el in query)
        {
            test[i] = new AirData()
            {
                room_number = (string)el.Element("Room"),
                time_record = (string)el.Element("Time"),
                temperature = (double)el.Element("Temperature"),
                humidity = (double)el.Element("Humidity")
            };
            i++;
        }
        return test;
    }

    //------------------------------ About Me ------------------------------------------------------
    [XmlRootAttribute(IsNullable = false)]
    public class MyData
    {
        public string name;
        public string id;
        [XmlArrayItem("Hobby")]
        public List<string> Hobbies = new List<string>();


        public MyData()
        {
            name = "Thakdanai Khunsaen";
            id = "5801012610059";
            Hobbies.Add("Reading");
            Hobbies.Add("Play Games");
        }
    }

    [WebMethod]
    public MyData AboutMe()
    {
        MyData data = new MyData();
        return data;

    }

    //------------------------------ Product Express ------------------------------------------------------
    public class ProductData
    {
        public string ProductID;
        public string CustomerName;
        public string Address;
        public double ProductWeight;
        public string ProductSended;
    }

    [WebMethod]
    public void AddProduct(string id, string customer_name, string address, double weight)
    {
        XElement xml = XElement.Load(AppDomain.CurrentDomain.BaseDirectory + "/Product.xml");
        xml.Add(new XElement("Product",
                    new XElement("ProductID", id),
                    new XElement("Customer", customer_name),
                    new XElement("Address", address),
                    new XElement("ProductWeight", weight),
                    new XElement("ProductSended", "Not Yet")
                    ));
        xml.Save(AppDomain.CurrentDomain.BaseDirectory + "/Product.xml");
    }

    [WebMethod]
    public void SendComplete(string id)
    {
        XElement xml = XElement.Load(AppDomain.CurrentDomain.BaseDirectory + "/Product.xml");
        IEnumerable<XElement> query =
            from el in xml.Elements()
            where (string)el.Element("ProductID") == id
            select el;
        foreach (XElement el in query)
        {
            el.Element("ProductSended").Value = "Complete";
        }
        xml.Save(AppDomain.CurrentDomain.BaseDirectory + "/Product.xml");
    }

    [WebMethod]
    [return: XmlRoot(ElementName = "Products")]
    public ProductData[] getProduct()
    {

        XElement xml = XElement.Load(AppDomain.CurrentDomain.BaseDirectory + "/Product.xml");
        IEnumerable<XElement> query =
            from el in xml.Elements()
            select el;

        ProductData[] data = new ProductData[query.Count()];

        int i = 0;
        foreach (XElement el in query)
        {
            data[i] = new ProductData()
            {
                ProductID = (string)el.Element("ProductID"),
                CustomerName = (string)el.Element("Customer"),
                Address = (string)el.Element("Address"),
                ProductWeight = (double)el.Element("ProductWeight"),
                ProductSended = (string)el.Element("ProductSended")
            };
            i++;
        }
        return data;
    }

    [WebMethod]
    [return: XmlRoot(ElementName = "Products")]
    public ProductData[] getProductBySendStatus(bool sended)
    {
        string status = "";
        if (sended) status = "Complete";
        else status = "Not Yet";

        XElement xml = XElement.Load(AppDomain.CurrentDomain.BaseDirectory + "/Product.xml");
        IEnumerable<XElement> query =
            from el in xml.Elements()
            where (string)el.Element("ProductSended") == status
            select el;

        ProductData[] data = new ProductData[query.Count()];

        int i = 0;
        foreach (XElement el in query)
        {
            data[i] = new ProductData()
            {
                ProductID = (string)el.Element("ProductID"),
                CustomerName = (string)el.Element("Customer"),
                Address = (string)el.Element("Address"),
                ProductWeight = (double)el.Element("ProductWeight"),
                ProductSended = (string) el.Element("ProductSended")
            };
            i++;
        }
        return data;
    }
}
